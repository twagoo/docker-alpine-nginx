= CLARIN nginx image
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

Docker image for creating nginx servers.
Processes are managed by supervisord and log aggregation is handled with fluentd (inherited from https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base[alpine base image]).

The image can be used either by extending it or by supplying host mounts for server configuration and data.

== Configuration

=== Inherited configuration

Configuration inherited from the https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base[alpine base image]:

=== Configuring nginx and VHOSTs
Supply VHOST configurations via a docker volume at /nginx_conf.d or directly override them at /etc/nginx/conf.d/. Do this either in your compose configuration or in the dockerfile which extends this image.
If no VHOSTs are supplied the server will not be functional.
In order to modify nginx main configuration file, override it via a docker volume at /etc/nginx/nginx.conf. 

=== Configuring SSL / TLS certificates

Supply a `server.crt` and `server.key` via a docker volume at /nginx_ssl.d or directly override the certificates at /etc/nginx/ssl/. Do this either in your compose configuration or in the dockerfile which extends this image.
If no certificates are supplied, this image will create a new self-signed certificate at startup.

