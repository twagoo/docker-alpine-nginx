#!/bin/bash

initialize() {
    echo "Initializing nginx container"
    if [ ! -f '/etc/nginx/ssl/server.crt' ] || [ ! -f '/etc/nginx/ssl/server.key' ]; then
        if [ ! -d '/nginx_ssl.d' ]; then
            # Generate new SSL certificates with a fresh start
            echo "No SSL certificate found for nginx. Generating new certificate."
            cd /tmp || exit 1
            openssl genrsa -des3 -passout 'pass:xxxxx' -out 'server.pass.key' 2048
            openssl rsa -passin 'pass:xxxxx' -in 'server.pass.key' -out 'server.key'
            rm 'server.pass.key'
            openssl req -new -key 'server.key' -out 'server.csr' -subj "/C=NL/ST=Gelderland/L=Nijmegen/O=CLARIN ERIC/OU=IT Department/CN=example.com"
            openssl x509 -req -days 365 -in 'server.csr' -signkey 'server.key' -out 'server.crt'
            mv 'server.crt' '/etc/nginx/ssl/server.crt'
            mv 'server.key' '/etc/nginx/ssl/server.key'
        else
            # Copy supplied SSL certificate
            echo "Copying supplied SSL certificate for nginx."
            cp '/nginx_ssl.d/server.crt' '/etc/nginx/ssl/server.crt'
            cp '/nginx_ssl.d/server.key' '/etc/nginx/ssl/server.key'
        fi
    fi

    chmod 0600 '/etc/nginx/ssl/server.key'

    if [ -d '/nginx_conf.d' ] && [ ! "$(find /etc/nginx/http.d/ -name \*.conf -type f  | wc -l)" -gt 0 ]; then  
        # Copy startup supplied configuration on fresh start
        echo "Copying nginx supplied extra configuration files."
        cp -r /nginx_conf.d/* '/etc/nginx/http.d/'
        chown -R nginx:nginx '/etc/nginx/http.d'
    fi
}

initialize
